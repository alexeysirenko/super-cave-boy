/// direction_move_bounce(collision_object)
collision_object = argument0;

// Collision
if (place_meeting(x + hspd, y, collision_object)) {
    while (!place_meeting(x + sign(hspd), y, collision_object)) {
        x += sign(hspd)
    }
    hspd = -(hspd/2); // bounce backward
    
}
x += hspd;

// Vertical collisions
if (place_meeting(x, y + vspd, collision_object)) {
    while (!place_meeting(x, y + sign(vspd), collision_object)) {
        y += sign(vspd)
    }
    vspd = -(vspd / 2);
    if (abs(vspd) < 2) vspd = 0; // Forces us to stop on low vspeed
}
y += vspd;
