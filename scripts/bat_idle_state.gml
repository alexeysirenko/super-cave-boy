/// bat_idle_state()
image_index = spr_bat_idle;

/// Look for the player and check distance to it
if (instance_exists(Player)) {
    var dist = point_distance(x, y, Player.x, Player.y);
    if (dist < sight) {
        state = bat_chase_state;
    }
}
