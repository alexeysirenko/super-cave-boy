/// move_state()
if (!place_meeting(x, y + 1, Solid)) {
    vspd += grav;
    
    // Player is in the air
    sprite_index = spr_player_jump;
    image_speed = 0; // not to animate, but use specific image by index (player jumps or lands)
    if (vspd > 0) {
        image_index = 1;
    } else {
        image_index = 0;
    }; // true/false = 1/0
    
    if (up_release && vspd < -6) {
        vspd = -6;
    }
} else {
    vspd = 0;
    
    // Jumping code    
    if (up) {
        vspd = -16;
        audio_play_sound(snd_jump, 5, false);
    }
    
    // Player is on the ground
    if (hspd == 0) {
        sprite_index = spr_player_idle;
    } else {
        sprite_index = spr_player_walk;
        image_speed = 0.6;
    }
}

if (right || left) {
    hspd_dir = right - left;
    hspd += hspd_dir * acc;
    if (hspd > spd) hspd = spd;
    if (hspd < -spd) hspd = -spd;   
} else {
    apply_friction(acc) // Friction. Slow down the player using acceleration value
}

if (hspd != 0) {
    image_xscale = sign(hspd);
}

// PLay the landing sound
if (place_meeting(x, y + vspd + 1, Solid) && vspd > 0) {
    audio_emitter_pitch(audio_em, random_range(0.8, 1.4));
    audio_emitter_gain(audio_em, 0.2);
    audio_play_sound_on(audio_em, snd_step, false, 6);
}

move(Solid);


/// Check for ledge grab state
var falling = y - yprevious > 0;
var SPRITE_HALF_WIDTH = 16;
var wasnt_wall = !position_meeting(x + (SPRITE_HALF_WIDTH + 1) * image_xscale, yprevious, Solid);
var is_wall = position_meeting(x + (SPRITE_HALF_WIDTH + 1) * image_xscale, y, Solid);

if (falling && wasnt_wall && is_wall) {
    hspeed = 0;
    vspeed = 0;
    
    // Move against the lege
    while (!place_meeting(x + image_xscale, y, Solid)) {
        x += image_xscale;
    }
    
    // Make sure we are the right height
    while (position_meeting(x + (SPRITE_HALF_WIDTH + 1) * image_xscale, y - 1, Solid)) {
        y -= 1;
    }
    
    sprite_index = spr_player_edge_grab;
    state = ledge_grab_state;
    
    // PLay ledge grab sound
    audio_emitter_pitch(audio_em, 1.5);
    audio_emitter_gain(audio_em, 0.1);
    audio_play_sound_on(audio_em, snd_step, false, 6);
}

